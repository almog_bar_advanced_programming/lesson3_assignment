#include "stdafx.h"
#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;

/*
clears results.
*/
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

/*
saves request's results into a vector.
*/
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

/*
performes purchase of a car from given account.
*/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	char str[90] = "select available from cars where id=";
	strcat_s(str, (std::to_string(carid)).c_str());
	int rc = sqlite3_exec(db, str, callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	std::unordered_map<string, vector<string>>::iterator it = results.find("available");
    vector<string> p = it->second;
	int available = stoi(p[0]);
	clearTable();
	if (available == 1)
	{
		char str[90] = "select price from cars where id=";
		strcat_s(str, (std::to_string(carid)).c_str());
		int rc = sqlite3_exec(db, str, callback, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}
		it = results.find("price");
		p = it->second;
		int price = stoi(p.front());
		clearTable();
		char str1[90] = "select balance from accounts where Buyer_id=";
		strcat_s(str1, (std::to_string(buyerid)).c_str());
		rc = sqlite3_exec(db, str1, callback, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}
		it = results.find("balance");
		p = it->second;
		int balance = stoi(p[0]);
		clearTable();
		if (balance >= price)
		{
			char str[90] = "update accounts set balance=";
			strcat_s(str, (std::to_string(balance-price)).c_str());
			strcat_s(str, " where Buyer_id=");
			strcat_s(str, (std::to_string(buyerid)).c_str());
			int rc = sqlite3_exec(db, str, NULL, 0, &zErrMsg);
			
			if (rc != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return false;
			}
			char str2[90] = "update cars set available=2 where id=";
			strcat_s(str2, (std::to_string(buyerid)).c_str());
			rc = sqlite3_exec(db, str2, NULL, 0, &zErrMsg);
			
			if (rc != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return false;
			}
			
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

/*
transfers money from one account to the other. 
*/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc = sqlite3_exec(db, "begin transaction", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	char str1[90] = "select balance from accounts where id=";
	strcat_s(str1, (std::to_string(from)).c_str());
	rc = sqlite3_exec(db, str1, callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	std::unordered_map<string, vector<string>>::iterator it = results.find("balance");
	vector<string> p = it->second;
	int balance = stoi(p.front());
	clearTable();

	char str[90] = "update accounts set balance=";
	strcat_s(str, (std::to_string(balance - amount)).c_str());
	strcat_s(str, " where id=");
	strcat_s(str, (std::to_string(from)).c_str());
	rc = sqlite3_exec(db, str, NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	char str4[90] = "select balance from accounts where id=";
	strcat_s(str4, (std::to_string(to)).c_str());
	rc = sqlite3_exec(db, str4, callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	it = results.find("balance");
	p = it->second;
	balance = stoi(p.front());
	clearTable();

	char str5[90] = "update accounts set balance=";
	strcat_s(str5, (std::to_string(balance + amount)).c_str());
	strcat_s(str5, " where id=");
	strcat_s(str5, (std::to_string(to)).c_str());
	rc = sqlite3_exec(db, str5, NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	rc = sqlite3_exec(db, "commit", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}

	return true;
}

/*
int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	//function1 examples
	if (carPurchase(5, 22, db, zErrMsg))
	{
		cout << "purchase successful" << endl;
	}
	else
	{
		cout << "purchase failed" << endl;
	}

	if (carPurchase(2, 15, db, zErrMsg))
	{
		cout << "purchase successful" << endl;
	}
	else
	{
		cout << "purchase failed" << endl;
	}

	if (carPurchase(9, 16, db, zErrMsg))
	{
		cout << "purchase successful" << endl;
	}
	else
	{
		cout << "purchase failed" << endl;
	}

	//function2 examples
	if (balanceTransfer(1, 2, 800, db, zErrMsg))
	{
		cout << "transfer successful" << endl;
	}
	else
	{
		cout << "transfer failed" << endl;
	}
	
	sqlite3_close(db);

}*/