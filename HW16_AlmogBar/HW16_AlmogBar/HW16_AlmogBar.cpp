// HW16_AlmogBar.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

int id;

/*
inserts last id into global variable - id
*/
int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	id = std::atoi(argv[0]);
	return 0;
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");

	//table creation
	rc = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name string)", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	//1st insert
	rc = sqlite3_exec(db, "insert into people(name) values(\"moshe\")", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	//2nd insert
	rc = sqlite3_exec(db, "insert into people(name) values(\"dana\")", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	
	//3rd insert
	rc = sqlite3_exec(db, "insert into people(name) values(\"rinat\")", NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	//retrieve of last id
	rc = sqlite3_exec(db, "select last_insert_rowid()", getLastId, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		//updating last id's name
		char str[90] = "update people set name=\"mimi\" where id=";
		strcat_s(str, (std::to_string(id)).c_str());
		rc = sqlite3_exec(db, str, NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			std::cout << "SQL error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
	}


	system("Pause");
	system("CLS");

	sqlite3_close(db);
    return 0;
}

